# Desafio BIX 2024 - Dev Laravel

Desafio de código para dev Laravel

## Requisitos

Debes tener instalado

-   Composer >= 2.5 -> https://getcomposer.org/download/

-   PHP >= 8.2 -> https://www.php.net/downloads.php

-   PostgreSQL >= 14.0 -> https://www.postgresql.org/download/

## Instalación

Primero deberas clonar el repositorio en tu maquina con los siguientes comandos:

```bash
  git clone https://gitlab.com/zadkiel-troche/desafio2024.git

  cd desafio2024
```

Luego ejecuta el siguiente comando para realizar la instalacion de dependencias:

```bash
  composer install
```

Una vez terminada la instalacion deberas copiar el archivo **.env.example** y pegarlo en el mismo directorio pero cambiarle el nombre a **.env**. Luego de esto deberas ejecutar el siguiente comando:

```bash
  php artisan key:generate
```

Ahora debe ajustar la configuración del archivo **.env** de la siguiente forma:

```bash
  DB_CONNECTION=pgsql
  DB_HOST=127.0.0.1
  DB_PORT=5432
  DB_DATABASE=desafio_bix
  DB_USERNAME=usuario
  DB_PASSWORD=constraseña
```

Luego deberá ejecutar la migración de la base de datos, pero antes debe crear una base de datos llamada desafio_bix y ejecutar el comando:

```bash
  php artisan migrate
  php artisan db:seed
```

## Iniciar servidor

Ahora podemos levantar el servidor con el siguinete comando:

```bash
  php artisan serve
```

El servidor se iniciara en http://127.0.0.1:8000/, en caso contrario podremos ver la URL en la que se inicio desde nuestra terminal de comandos.
