<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Documento extends Model
{
    use HasFactory;

    protected $table = 'adm_documentos';

    protected $fillable = [
        'numero',
        'es_principal',
        'adm_persona_id',
    ];

    /**
     * Get the persona that owns the document.
     */
    public function persona()
    {
        return $this->belongsTo(Persona::class, 'adm_persona_id');
    }
}
