<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Perfil extends Model
{
    use HasFactory;

    protected $table = 'adm_perfiles';

    protected $fillable = [
        'activo',
        'descripcion',
        'user_id',
        'adm_rol_id',
    ];

    /**
     * Get the usuario record associated with the perfil.
     */
    public function usuario()
    {
        return $this->belongsTo(Usuario::class, 'user_id');
    }

    /**
     * Get the rol record associated with the perfil.
     */
    public function rol()
    {
        return $this->belongsTo(Rol::class, 'adm_rol_id');
    }
}
