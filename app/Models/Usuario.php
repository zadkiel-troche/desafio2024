<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    use HasFactory;
    
    protected $table = 'adm_usuarios';

    protected $fillable = [
        'name',
        'email',
        'password',
        'activo',
        'bloqueado',
        'adm_persona_id',
    ];

    /**
     * Get the persona record associated with the user.
     */
    public function persona()
    {
        return $this->belongsTo(Persona::class, 'adm_persona_id');
    }
}
