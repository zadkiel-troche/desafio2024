<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Usuario>
 */
class UsuarioFactory extends Factory
{
    /**
     * The current password being used by the factory.
     */
    protected static ?string $password;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        
        $password = $this->generateValidPassword();

        return [
            'name' => $this->faker->name(),
            'email' => $this->faker->unique()->safeEmail(),
            'password' => $password,
            'activo' => $this->faker->boolean(80), // 80% de los usuarios estarán activos
        ];
    }

    /**
     * Generate a valid password.
     *
     * @return string
     */
    protected function generateValidPassword(): string
    {
        $uppercase = Str::upper($this->faker->randomLetter());
        $lowercase = Str::lower($this->faker->randomLetter());
        $digits = $this->faker->randomDigit();
        $specialChar = $this->faker->randomElement(['*', '.', '_']);

        // Generate a random password
        $password = $this->faker->shuffle($uppercase . $lowercase . $digits . $specialChar . Str::random(4, 8));

        // Ensure the password meets the length requirement
        if (Str::length($password) < 8) {
            $password .= Str::random(8 - Str::length($password));
        } elseif (Str::length($password) > 16) {
            $password = Str::limit($password, 16, '');
        }

        return Hash::make($password);
    }
}
