<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('adm_documentos', function (Blueprint $table) {
            $table->id()->primary();
            $table->string('numero');
            $table->boolean('es_principal');
            $table->timestamps();

            //Clave foranea
            $table->unsignedBigInteger('adm_persona_id');
            //definicion de la clave foranea
            $table->foreign('adm_persona_id')->references('id')->on('adm_personas');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('adm_documentos');
    }
};
