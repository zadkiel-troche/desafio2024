<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('adm_perfiles', function (Blueprint $table) {
            $table->id()->primary();
            $table->boolean('activo')->default(true);
            $table->text('descripcion');
            $table->timestamps();

            //Clave foranea con usuarios
            $table->unsignedBigInteger('user_id');
            //definicion de la clave foranea
            $table->foreign('user_id')->references('id')->on('adm_usuarios');

            //Clave foranea con roles
            $table->unsignedBigInteger('adm_rol_id');
            //definicion de la clave foranea
            $table->foreign('adm_rol_id')->references('id')->on('adm_roles');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('adm_perfiles');
    }
};
