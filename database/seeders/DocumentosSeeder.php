<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Documento;
use App\Models\Persona;

class DocumentosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $personas = Persona::all();

        $personas->each(function ($persona) {
            $documentosCount = rand(1, 5); // Cada persona tendrá entre 1 y 5 documentos
            Documento::factory()->count($documentosCount)->create(['adm_persona_id' => $persona->id]);
        });
    }
}
