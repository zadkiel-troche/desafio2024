<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Usuario;
use App\Models\Rol;
use App\Models\Perfil;

class PerfilesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $usuarios = Usuario::all();
        $roles = Rol::all();

        $usuarios->each(function ($usuario) use ($roles) {
            $roles->random(rand(1, 3))->each(function ($rol) use ($usuario) {
                Perfil::factory()->create([
                    'user_id' => $usuario->id,
                    'adm_rol_id' => $rol->id,
                ]);
            });
        });
    }
}
