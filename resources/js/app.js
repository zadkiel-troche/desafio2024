import "./bootstrap";
import { createApp } from "vue";

const app = createApp({});

import LoginComponent from "./components/login.vue";

app.component("login-component", LoginComponent);

app.mount("#app");
