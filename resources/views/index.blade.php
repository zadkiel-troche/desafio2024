@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Bienvenido {{ Auth::user()->name }}</div>

                    <div class="card-body">
                        <p>Perfiles:</p>
                        <ul>
                            @foreach(Auth::user()->perfiles as $perfil)
                                <li>{{ $perfil }}</li>
                            @endforeach
                        </ul>
                        <form action="{{ route('logout') }}" method="POST">
                            @csrf
                            <button type="submit" class="btn btn-primary">Cerrar sesión</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
