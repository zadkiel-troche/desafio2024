@extends('layouts.template')

@section('content')
    <div id="app">
        <login-component />
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/app.js') }}" defer></script>
@endsection