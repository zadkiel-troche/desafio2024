<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/* Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');
 */


Route::middleware('auth:api')->group(function () {
    Route::get('/users', 'UserController@index');
    Route::put('/users/change-password', 'UserController@changePassword');
    Route::delete('/users/delete-bulk', 'UserController@deleteBulk');
});
