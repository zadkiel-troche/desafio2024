<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('index');
})->middleware('auth'); // Middleware de autenticación

Route::get('/login', function () {
    return view('login');
})->name('login'); // Ruta para mostrar el formulario de inicio de sesión

Route::post('/login', 'Auth\LoginController@login'); // Ruta para el inicio de sesión
Route::post('/logout', 'Auth\LoginController@logout'); // Ruta para el cierre de sesión

